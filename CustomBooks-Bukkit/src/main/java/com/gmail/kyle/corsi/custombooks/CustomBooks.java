package com.gmail.kyle.corsi.custombooks;

import java.util.logging.Level;
import java.util.logging.Logger;

import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.command.ConsoleCommandSender;
import org.bukkit.entity.Player;
import org.bukkit.event.HandlerList;
import org.bukkit.plugin.PluginManager;
import org.bukkit.plugin.java.JavaPlugin;

import net.md_5.bungee.api.ChatColor;

public final class CustomBooks extends JavaPlugin {
    //Global Variables
    private Logger _log;
    private PlayerJoinEventListener _playerListener;

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        if (command.getName().equalsIgnoreCase("book")) {
            if (args.length > 0) {
                if (args[0].equalsIgnoreCase("reload")) {
                    if (sender instanceof Player) {
                        Player player = (Player) sender;
                        if (player.hasPermission("custombooks.command.reload")) {
                            player.sendMessage(ChatColor.GREEN + "Reloading Custom Books Config");
                            HandlerList.unregisterAll(_playerListener);
                            PluginManager pluginManager = Bukkit.getServer().getPluginManager();
                            pluginManager.registerEvents(_playerListener, this);
                            try {
                                //PluginManager pluginManager = Bukkit.getServer().getPluginManager();
                                //HandlerList.unregisterAll(_playerListener);
                                //_playerListener = new PlayerJoinEventListener(_log, this.getConfig());
                                this.getConfig().options().copyDefaults(true);
                                //pluginManager.registerEvents(_playerListener, this);
                                _playerListener = new PlayerJoinEventListener(_log, this.getConfig());
                                //_playerListener.setFileConfiguration(this.getConfig());
                                //_playerListener.generateBook();
                                player.sendMessage(ChatColor.GREEN + "Custom Books Config reloaded successfully");
                                return true;
                            } catch (IllegalArgumentException | SecurityException ex) {
                                player.sendMessage(ChatColor.RED + "Custom Books not reloaded. Check console for errors");
                                _log.log(Level.SEVERE, ex.getMessage());
                                return false;
                            }
                        } else {
                            return false;
                        }
                    } else if (sender instanceof ConsoleCommandSender) {
                        ConsoleCommandSender console = (ConsoleCommandSender) sender;
                        console.sendMessage(ChatColor.GREEN + "Reloading Custom Books Config");
                        try {
                            _playerListener.generateBook();
                            console.sendMessage("Custom Books Config reloaded successfully");
                            return true;
                        } catch (IllegalArgumentException | IllegalAccessException | NoSuchFieldException
                                | SecurityException ex) {
                            console.sendMessage(ChatColor.RED + "Custom Books not reloaded");
                            _log.log(Level.SEVERE, ex.getMessage());
                            return false;
                        }
                    } else {
                        sender.sendMessage("You must be a player or server console");
                        return false;
                    }
                } else {
                    sender.sendMessage(ChatColor.RED + "Unknown option");
                    return false;
                }
            } else {
                if (sender instanceof Player) {
                    Player player = (Player) sender;
                    if (player.hasPermission("custombooks.command.use")) {
                        _playerListener.openBook(player);
                        return true;
                    } else {
                        return false;
                    }
                } else {
                    sender.sendMessage("You must be a player!");
                    return false;
                }
            }
        } else if (command.getName().equalsIgnoreCase("console")) {
            if (sender instanceof Player) {
                if (args.length > 0) {
                    String commandString = "";
                    Player player = (Player) sender;
                    for (String arg : args) {
                        if (arg.contains("@p")) {
                            arg = arg.replace("@p", player.getName());
                        }
                        commandString += arg + " ";
                    }
                    _log.log(Level.INFO, "Executing:" + commandString);
                    Bukkit.getServer().dispatchCommand(Bukkit.getServer().getConsoleSender(), commandString);
                    return true;
                } else {
                    _log.log(Level.SEVERE, "No command information send");
                    return false;
                }
            } else {
                sender.sendMessage("You must be a player to use this command");
                return false;
            }
        } else {
            return false;
        }
    }

    @Override
    public void onEnable() {
        _log = Bukkit.getLogger();
        _playerListener = new PlayerJoinEventListener(_log, this.getConfig());
        this.getConfig().options().copyDefaults(true);
        this.saveConfig();
        PluginManager pluginManager = Bukkit.getServer().getPluginManager();
        pluginManager.registerEvents(_playerListener, this);
    }

    @Override
    public void onDisable() {

    }
}