package com.gmail.kyle.corsi.custombooks;

import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.bukkit.Material;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.craftbukkit.v1_12_R1.entity.CraftPlayer;
import org.bukkit.craftbukkit.v1_12_R1.inventory.CraftMetaBook;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.BookMeta;
import org.bukkit.scheduler.BukkitRunnable;

import io.netty.buffer.ByteBuf;
import io.netty.buffer.Unpooled;
import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.chat.ClickEvent;
import net.md_5.bungee.api.chat.ComponentBuilder;
import net.md_5.bungee.api.chat.HoverEvent;
import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.chat.ComponentSerializer;
import net.minecraft.server.v1_12_R1.IChatBaseComponent;
import net.minecraft.server.v1_12_R1.PacketDataSerializer;
import net.minecraft.server.v1_12_R1.PacketPlayOutCustomPayload;

public class PlayerJoinEventListener implements Listener {
    //Global Variables
    private Logger _log;
    private FileConfiguration _config;
    private ItemStack _book = new ItemStack(Material.WRITTEN_BOOK);

    public PlayerJoinEventListener(Logger log, FileConfiguration config) {
        _log = log;
        _config = config;
        try {
			generateBook();
		} catch (IllegalArgumentException | IllegalAccessException | NoSuchFieldException | SecurityException ex) {
			_log.log(Level.SEVERE, ex.getMessage());
		}
    }

    public void setFileConfiguration(FileConfiguration config) {
        _config = config;
    }

    @EventHandler
    public void onJoin(PlayerJoinEvent event) {
        Player joinedPlayer = event.getPlayer();
        new BukkitRunnable(){
            @Override
            public void run() {
                if (joinedPlayer.hasPermission("custombooks.onjoin.receive"))
                    openBook(joinedPlayer);
            }
        }.runTaskLater(CustomBooks.getPlugin(CustomBooks.class), 5);
    }

	public void openBook(Player joinedPlayer) {
        int slot = joinedPlayer.getInventory().getHeldItemSlot();
        ItemStack old = joinedPlayer.getInventory().getItem(slot);
        joinedPlayer.getInventory().setItem(slot, _book);
        ByteBuf buf = Unpooled.buffer(256);
        buf.setByte(0, (byte)0);
        buf.writerIndex(1);
        PacketPlayOutCustomPayload packet = new PacketPlayOutCustomPayload("MC|BOpen", new PacketDataSerializer(buf));
        ((CraftPlayer)joinedPlayer).getHandle().playerConnection.sendPacket(packet);
        joinedPlayer.getInventory().setItem(slot, old);
    }
    
    public void generateBook() throws IllegalArgumentException, IllegalAccessException, NoSuchFieldException, SecurityException {
        BookMeta bookMeta = (BookMeta) _book.getItemMeta();
        bookMeta.setTitle(_config.getString("book-title"));
        bookMeta.setAuthor(_config.getString("book-author"));
        List<IChatBaseComponent> pages = (List<IChatBaseComponent>) CraftMetaBook.class.getDeclaredField("pages").get(bookMeta);
        pages.clear();
        for(int i = 1; i < Integer.MAX_VALUE; i++) {
            //Check if page is valid
            if (_config.get("pages.page" + i) != null) {
                TextComponent page = new TextComponent();
                for (int j = 1; j < Integer.MAX_VALUE; j++) {
                    if (_config.get("pages.page" + i + ".line" + j) != null) {
                        String text = ChatColor.translateAlternateColorCodes('&', _config.getString("pages.page" + i + ".line" + j + ".text"));
                        //ChatColor color = ChatColor.getByChar('&');
                        //_log.log(Level.INFO, "Found color:" + (color == null ? "null" : color.toString()));
                        Boolean clickable = _config.getBoolean("pages.page" + i + ".line" + j + ".clickable");
                        String clickEvent = _config.getString("pages.page" + i + ".line" + j + ".click-event");
                        String clickAction = _config.getString("pages.page" + i + ".line" + j + ".click-action");
                        Boolean hoverover = _config.getBoolean("pages.page" + i + ".line" + j + ".hoverover");
                        String hoveroverText = ChatColor.translateAlternateColorCodes('&', _config.getString("pages.page" + i + ".line" + j + ".hoverover-text"));
                        TextComponent line = new TextComponent(text);
                        if (clickable) {
                            switch (clickEvent.toLowerCase()) {
                                case "open_url":
                                    line.setClickEvent(new ClickEvent(ClickEvent.Action.OPEN_URL, clickAction));
                                    break;
                                case "run_command":
                                    line.setClickEvent(new ClickEvent(ClickEvent.Action.RUN_COMMAND, clickAction));
                                    break;
                                case "change_page":
                                    line.setClickEvent(new ClickEvent(ClickEvent.Action.CHANGE_PAGE, clickAction));
                                    break;
                            }
                        }
                        if (hoverover) {
                            line.setHoverEvent(new HoverEvent(HoverEvent.Action.SHOW_TEXT, new ComponentBuilder(hoveroverText).create()));
                        }
                        page.addExtra(line);
                    } else {
                        break;
                    }
                }
                pages.add(IChatBaseComponent.ChatSerializer.a(ComponentSerializer.toString(page)));
            } else {
                break;
            }
        }
        _book.setItemMeta(bookMeta);
    }
}