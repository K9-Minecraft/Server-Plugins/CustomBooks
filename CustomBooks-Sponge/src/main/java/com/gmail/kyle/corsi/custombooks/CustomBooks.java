package com.gmail.kyle.corsi.custombooks;

import java.nio.file.Path;

import com.google.inject.Inject;

import org.slf4j.Logger;
import org.spongepowered.api.Game;
import org.spongepowered.api.Sponge;
import org.spongepowered.api.command.spec.CommandSpec;
import org.spongepowered.api.config.ConfigDir;
import org.spongepowered.api.event.Listener;
import org.spongepowered.api.event.game.state.GameInitializationEvent;
import org.spongepowered.api.event.game.state.GameStartedServerEvent;
import org.spongepowered.api.event.network.ClientConnectionEvent;
import org.spongepowered.api.plugin.Plugin;
import org.spongepowered.api.plugin.PluginContainer;
import org.spongepowered.api.text.BookView;
import org.spongepowered.api.text.Text;

@Plugin(id="custombooks", name="CustomBooks", version="1.0.0")
public final class CustomBooks {
    /**
     * Injected configuration directory for the plugin
     */
    @Inject
    @ConfigDir(sharedRoot = false)
    private Path configDirectory;
    @Inject
    private Game _game;
    @Inject
    private Logger _logger;
    @Inject
    private PluginContainer _container;

    private BookView _bookView;

    private CustomBookCommand _bookCommand;

    @Listener
    public void onServerStart(GameStartedServerEvent event) {
        this._game.getScheduler().createAsyncExecutor(this).execute(this::registerCommands);
    }

    @Listener
    public void onGameInitialized(GameInitializationEvent event) {
        this._game.getScheduler().createAsyncExecutor(this).execute(this::checkForUpdates);
    }

    @Listener
    public void onPlayerJoin(ClientConnectionEvent.Join event) {
        _bookCommand.sendBookView(event.getTargetEntity());
    }

    private void registerCommands() {
        _bookCommand = new CustomBookCommand(_logger, configDirectory, _container);

        CommandSpec bookReloadCommand = CommandSpec.builder()
        .description(Text.of("Reloads the plugin based on the config file"))
        .permission("custombooks.command.reload")
        .executor(_bookCommand.ReloadCommand)
        .build();

        CommandSpec bookBaseCommand = CommandSpec.builder()
        .description(Text.of("Displays the defined book to the player"))
        .permission("custombooks.command.use")
        .child(bookReloadCommand, "reload")
        .executor(_bookCommand.BaseCommand)
        .build();

        Sponge.getCommandManager().register(this, bookBaseCommand, "book");
    }

    private void checkForUpdates() {
        //TBD
    }
}