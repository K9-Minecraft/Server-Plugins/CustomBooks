package com.gmail.kyle.corsi.custombooks;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.List;

import org.slf4j.Logger;
import org.spongepowered.api.asset.Asset;
import org.spongepowered.api.command.CommandException;
import org.spongepowered.api.command.CommandResult;
import org.spongepowered.api.command.CommandSource;
import org.spongepowered.api.command.args.CommandContext;
import org.spongepowered.api.command.spec.CommandExecutor;
import org.spongepowered.api.entity.living.player.Player;
import org.spongepowered.api.plugin.PluginContainer;
import org.spongepowered.api.text.BookView;
import org.spongepowered.api.text.LiteralText;
import org.spongepowered.api.text.Text;
import org.spongepowered.api.text.action.TextActions;
import org.spongepowered.api.text.format.TextColors;
import org.spongepowered.api.text.serializer.TextSerializers;

import ninja.leaping.configurate.ConfigurationNode;
import ninja.leaping.configurate.commented.CommentedConfigurationNode;
import ninja.leaping.configurate.hocon.HoconConfigurationLoader;
import ninja.leaping.configurate.loader.ConfigurationLoader;
import ninja.leaping.configurate.objectmapping.ObjectMappingException;

public class CustomBookCommand {

    private Logger _logger;
    private BookView _bookView;
    private Path _configDirectory;
    private PluginContainer _container;

    public CustomBookCommand(Logger logger, Path configDirectory, PluginContainer container) {
        _logger = logger;
        _configDirectory = configDirectory;
        _container = container;
        try {
			readConfig();
		} catch (MalformedURLException e) {
			_logger.error("Error reading config file:" + e.getMessage());
		}
    }

    
	public CommandExecutor BaseCommand = new CommandExecutor(){
        @Override
        public CommandResult execute(CommandSource src, CommandContext args) throws CommandException {
            Player player = (Player) src;
            sendBookView(player);
            return null;
        }
    };

    public void sendBookView(Player player) {
        player.sendBookView(_bookView);
    }
    
    public CommandExecutor ReloadCommand = new CommandExecutor(){
        @Override
        public CommandResult execute(CommandSource src, CommandContext args) throws CommandException {
            Player player = (Player) src;
            player.sendMessage(Text.builder().append(Text.of("Reloading config")).color(TextColors.GREEN).build());
            try {
                readConfig();
                player.sendMessage(Text.builder().append(Text.of("Reload complete")).color(TextColors.GREEN).build());
                return CommandResult.success();
			} catch (MalformedURLException e) {
                _logger.error("Error reading config file:" + e.getMessage());
                player.sendMessage(Text.builder().append(Text.of("Could not reload CustomBook config. Please check server logs for more information.")).color(TextColors.RED).build());
                return null;
			}
        }
    };
    
    public void generateBook(ConfigurationNode root) throws MalformedURLException, ObjectMappingException {
        String title = root.getNode("book-title").getString();
        String author = root.getNode("book-author").getString();
        BookView.Builder bookBuilder = BookView.builder()
        .title(Text.of(title))
        .author(Text.of(author));
        List<? extends ConfigurationNode> pageChildren = root.getNode("pages").getChildrenList();
        for (ConfigurationNode page : pageChildren) {
            _logger.info(page.getString());
            List<? extends ConfigurationNode> lineChildren = page.getNode("lines").getChildrenList();
            Text.Builder configuredLine = LiteralText.builder();
            for (ConfigurationNode line : lineChildren) {
                _logger.info(line.getString());
                String text = line.getNode("text").getString();
                Boolean clickable = line.getNode("clickable").getBoolean();
                String clickEvent = line.getNode("click-event").getString();
                String clickAction = line.getNode("click-action").getString();
                Boolean hoverover = line.getNode("hoverover").getBoolean();
                String hoveroverText = line.getNode("hoverover-text").getString();
                Text.Builder lineBuilder = Text.builder().append(TextSerializers.FORMATTING_CODE.deserialize(text));
                if (clickable) {
                    switch (clickEvent) {
                        case "OPEN_URL":
                            lineBuilder.onClick(TextActions.openUrl(new URL(clickAction)));
                        break;
                        case "RUN_COMMAND":
                            lineBuilder.onClick(TextActions.runCommand(clickAction));
                        break;
                        case "CHANGE_PAGE":
                            lineBuilder.onClick(TextActions.changePage(Integer.parseInt(clickAction)));
                        break;
                    }
                }
                if (hoverover) {
                    lineBuilder.onHover(TextActions.showText(Text.of(TextSerializers.FORMATTING_CODE.deserialize(hoveroverText))));
                }
                configuredLine.append(lineBuilder.build());
                configuredLine.append(LiteralText.NEW_LINE);
            }
            bookBuilder.addPage(configuredLine.build());
        }
        _bookView = bookBuilder.build();
    }

    public void readConfig() throws MalformedURLException {
        Path path = _configDirectory.resolve("config.yml");
        _logger.info("Checking for config file in:" + path.toString());
        if (Files.notExists(path)) {
            Asset jarConfigFile = _container.getAsset("config.yml").get();
            try {
				jarConfigFile.copyToFile(path);
			} catch (IOException e1) {
				_logger.error("Unable to copy default config:" + e1.getMessage());
			}
        }
        ConfigurationLoader<CommentedConfigurationNode> loader = HoconConfigurationLoader.builder().setPath(path).build();
        try {
            ConfigurationNode root = loader.load();
            generateBook(root);
        } catch (IOException e) {
            _logger.error("Error reading config file:" + e.getMessage());
        } catch (ObjectMappingException e) {
			_logger.error("Error reading config file:" + e.getMessage());
		}
    }
}